import React from 'react';

import AddOption from './AddOption';
import Options from './Options';
import Action from './Action';
import Header from './Header';
import OptionModal from './OptionModal';

export default class IndecisionApp extends React.Component {
  state = {
    options: [],
    selectedOption: undefined,
  };

  componentDidMount() {
    try {
      const json = localStorage.getItem('options');
      const options = JSON.parse(json);
      if (options) {
        this.setState(() => ({ options }));
      }
    } catch (e) {
      // Do nothing
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.options.length === this.state.options.length) return;

    const json = JSON.stringify(this.state.options);
    localStorage.setItem('options', json);
  }

  componentWillUnmount() {
    console.log('componentWillUnmount');
  }

  handleClearSelectedOption = () => {
    this.setState(() => ({
      selectedOption: undefined,
    }));
  };

  handleDeleteOptions = () => {
    this.setState(() => ({
      options: [],
    }));
  };

  handlePick = () => {
    const { options } = this.state;
    const randomNum = Math.floor(Math.random() * options.length);
    const option = options[randomNum];
    this.setState(() => ({
      selectedOption: option,
    }));
  };

  handleAddOption = option => {
    if (!option) {
      return 'Enter value to add item';
    } else if (this.state.options.indexOf(option) > -1) {
      return 'This option already exists';
    }

    this.setState(state => ({
      options: [...state.options, option],
    }));
  };

  handleDeleteOption = optionToRemove => {
    this.setState(state => ({
      options: state.options.filter(option => optionToRemove !== option),
    }));
  };

  render() {
    const subtitle = 'Put your life in the hands of a computer';
    const { options, selectedOption } = this.state;

    return (
      <div>
        <Header subtitle={subtitle} />
        <div className='container'>
          <Action
            hasOptions={options.length > 0}
            handlePick={this.handlePick}
          />
          <div className='widget'>
            <Options
              options={options}
              handleDeleteOptions={this.handleDeleteOptions}
              handleDeleteOption={this.handleDeleteOption}
            />
            <AddOption handleAddOption={this.handleAddOption} />
          </div>
          <OptionModal
            selectedOption={selectedOption}
            handleClearSelectedOption={this.handleClearSelectedOption}
          />
        </div>
      </div>
    );
  }
}
