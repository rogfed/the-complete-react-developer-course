class Counter extends React.Component {
  constructor(props) {
    super(props);
    this.handleAddOne = this.handleAddOne.bind(this);
    this.handleMinusOne = this.handleMinusOne.bind(this);
    this.handleReset = this.handleReset.bind(this);

    this.state = {
      count: 0,
    };
  }

  componentDidMount() {
    const stringCount = localStorage.getItem('count');
    const count = parseInt(stringCount);
    if (isNaN(count)) return;
    this.setState(() => ({ count }));
  }

  componentDidUpdate(prevProps, prevState) {
    const { count } = this.state;
    if (prevState.count === count) return;
    localStorage.setItem('count', count);
  }

  handleAddOne() {
    this.setState(state => ({
      count: state.count + 1,
    }));
  }

  handleMinusOne() {
    this.setState(state => ({
      count: state.count - 1,
    }));
  }

  handleReset() {
    this.setState(() => ({
      count: 0,
    }));
  }

  render() {
    const { count } = this.state;
    return (
      <div>
        <h1>Count: {count}</h1>
        <button onClick={this.handleAddOne}>+1</button>
        <button onClick={this.handleMinusOne}>-1</button>
        <button onClick={this.handleReset}>reset</button>
      </div>
    );
  }
}

ReactDOM.render(<Counter />, document.getElementById('app'));
