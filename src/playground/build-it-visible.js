class VisibilityToggle extends React.Component {
  constructor(props) {
    super(props);

    this.handleToggleVisibility = this.handleToggleVisibility.bind(this);

    this.state = {
      visibility: false,
    };
  }

  handleToggleVisibility() {
    this.setState(state => ({
      visibility: !state.visibility,
    }));
  }

  render() {
    const { visibility } = this.state;

    return (
      <div>
        <h1>Visibility Toogle</h1>
        <button onClick={this.handleToggleVisibility}>
          {visibility ? 'Hide Details' : 'Show Details'}
        </button>
        {visibility && <p>Hey, These are some details you can now see!</p>}
      </div>
    );
  }
}

const root = document.getElementById('app');

ReactDOM.render(<VisibilityToggle />, root);

// let showDetails = false;

// const toggleDetails = () => {
//   showDetails = !showDetails;
//   render();
// };

// const render = () => {
//   const VisibilityComponent = (
//     <div>
//       <h1>Visibility Toogle</h1>
//       <button onClick={toggleDetails}>
//         {showDetails ? 'Hide Details' : 'Show Details'}
//       </button>
//       {showDetails && <p>Hey, These are some details you can now see!</p>}
//     </div>
//   );

//   const root = document.getElementById('app');

//   ReactDOM.render(VisibilityComponent, root);
// };

// render();
